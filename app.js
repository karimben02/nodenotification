'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
    res.send('Push App');
});

app.use(express.static('public'));

app.listen(PORT, HOST);
console.log(`Push server start`);

app.post("/api/save/subscription", (req, res) =>{
    console.log(req)
    res.setHeader('Content-Type','application/json')
    res.send(JSON.stringify({data: {success: true}}))
    return res
})