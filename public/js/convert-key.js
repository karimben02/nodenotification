const private_key = "-9eupxAdGn4N3ilso2VjFEft81NLeafZlePEXH62Z6g";
const public_key = "BKLh7Mx6U15fkXe77QgZOQSlucUTihCqSkN7HzJlgxVQV4TbjrFvSOzbahp56F0-B4pXfR6K8nmYyq0NsTaqQbU";

/**
 * Conversion de la clef VAPID pour la subscription
 * @param base64String
 * @returns {Uint8Array}
 */
 function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');
 
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
 
    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}