function registerCheckBrowser() {
    return navigator.serviceWorker.register('/js/service-worker.js').then(function (reg) {
        var options = {
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array(public_key)
          };
          reg.pushManager.subscribe(options).then(
            function(pushSubscription) {
              console.log(pushSubscription.endpoint);
              
              req.open("POST","/api/save/subscription");
              req.send(pushSubscription.endpoint)
              // The push subscription details needed by the application
              // server are now available, and can be sent to it using,
              // for example, an XMLHttpRequest.
            }, function(error) {
              // During development it often helps to log errors to the
              // console. In a production environment it might make sense to
              // also report information about errors back to the
              // application server.
              console.log(error);
            });
        // registration worked
        console.log('Registration succeeded. Scope is ' + reg.scope);
        return reg;
    }).catch(function (error) {
        // registration failed
        console.log('Registration failed with ' + error);
    });
}

function checkBrowser() {
return ('serviceWorker' in navigator)
}


const private_key = "-9eupxAdGn4N3ilso2VjFEft81NLeafZlePEXH62Z6g";
const public_key = "BKLh7Mx6U15fkXe77QgZOQSlucUTihCqSkN7HzJlgxVQV4TbjrFvSOzbahp56F0-B4pXfR6K8nmYyq0NsTaqQbU";

/**
 * Conversion de la clef VAPID pour la subscription
 * @param base64String
 * @returns {Uint8Array}
 */
 function urlBase64ToUint8Array(tets) {
    const padding = '='.repeat((4 - tets.length % 4) % 4);
    const base64 = (tets + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');
 
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
 
    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

let req = new XMLHttpRequest();